const express = require('express')
const path = require('path')
const dotenv = require('dotenv')
const shoppingItemRouter = require('./routes/shopping-item/shopping-item.route')

/** Carga de variables de ambiente */
dotenv.config({ path: './src/environments/.env', encoding: 'utf8' })
const appPort = process.env.APP_PORT || 3000

/** Inicializacion de express */
const server = express()
server.use(express.json())

/** Implementacion de las rutas estaticas */
console.log('path: ', __dirname)
server.use('/static', express.static(path.join(__dirname, '../public/images')))

/** Implementacion de los routers */
server.use('/shopping', shoppingItemRouter)

/** Inicializacion del servidor */
server.listen(appPort, () => {
  console.log(`Aplicacion iniciada desde el puerto ${appPort}`)
})
