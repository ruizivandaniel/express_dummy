const express = require('express')
const validateTokenMidd = require('./../../middlewares/validate-token.middleware')
const { singleUploadMidd, arrayUploadMidd } = require('../../utils/file-uploader.util')
const shoppingItemSchema = require('../../validations/shopping-item.validation')
const multer = require('multer')

/** Inicializacion del router. */
const _router = express.Router()

/**
 * Ejemplo de middleware GLOBAL. Esto sirve para esas
 * funciones que necesites correr en toda la ruta.
 */
_router.all('*', validateTokenMidd)

/** Declaracion de metodos de la ruta. */
_router.post('/', (req, res) => {
  /**
   * Implementacion del uploader. En la pagina oficial
   * el ejemplo es distinto, pero de esta manera es
   * posible poder validar en caso de que haya un error
   * con la carga del archivo (cosa que la original no
   * deja).
   */
  singleUploadMidd(req, res, async err => {
    if (err instanceof multer.MulterError) {
      /** Error original de multer. */
      res.status(400).send({
        message: 'Error al cargar el archivo.',
        error: err
      })
      return
    } else if (err) {
      /** Error desconocido de multer. */
      res.status(500).send({
        message: 'Error desconocido al cargar el archivo.',
        error: err
      })
      return
    }

    /**
     * Toda la info cargada hasta el momento
     */
    console.log('file: ', req.file) // Este campo se añade con el middleware de multer.
    console.log('metadata: ', res.locals.userMetadata) // Este campo se añade con el middleware "validateTokenMidd"
    console.log('body: ', req.body) // body original del request.

    /**
     * Aca deberias validar que los campos que
     * necesitas existan y sean validos. La libreria
     * te valida los campos y en caso de ser valido
     * te devuelve el objeto completo (dependiendo)
     * que seteaste en el schema.
     * En este caso guardo el resultado en la variable
     * "objToPersist". En caso de error, catcheo y mando
     * el mensaje del error que devuelve Joi (se puede
     * formatear los mensajes de error).
     *
     * MAS INFO: https://joi.dev/api/?v=17.13.3
     */
    let objToPersist = {}
    try {
      objToPersist = await shoppingItemSchema.validateAsync({ ...req.body, fileName: req.file.filename })
    } catch (err) {
      res.status(400).send({
        message: 'Request invalido',
        error: err.message
      })
      return
    }

    /**
     * Aca ya te quedaria el objeto que tendrias
     * que guardar en la base de datos. Depende
     * que ORM utilices deberias crearlo en una
     * carpeta
     */
    console.log('objToPersist: ', objToPersist)
    // shoppingItem.create(objToPersist);

    /**
     * Si llegaste aca ya tenes todo ok, por
     * lo que devolves el 200.
     */
    res.status(200).send({
      message: 'Item cargado correctamente'
    })
  })
})

/** Ejemplo de como podrias recibir multiples imagenes. */
_router.post('/multiple', (req, res) => {
  arrayUploadMidd(req, res, err => {
    if (err instanceof multer.MulterError) {
      /** Error original de multer. */
      res.status(400).send({
        message: 'Error al cargar el archivo.',
        error: err
      })
      return
    } else if (err) {
      /** Error desconocido de multer. */
      res.status(500).send({
        message: 'Error desconocido al cargar el archivo.',
        error: err
      })
      return
    }
    console.log('metadata: ', res.locals.userMetadata)
    console.log('body: ', req.body)
    console.log('files: ', req.files) // FIJATE LA S AL FINAL!!!
    res.status(200).send({ message: 'Archivo cargado correctamente' })
  })
})

/** Exporto el singleton del router, con freeze para que sea solo "readonly" (seguridad) */
module.exports = _router
