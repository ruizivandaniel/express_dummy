const multer = require('multer')

/**
 * MAS INFO: https://www.npmjs.com/package/multer
 */

/** Opciones del storage de la imagen */
const _storage = multer.diskStorage({
  /** Seteo la ruta donde se guardaran las imagenes. */
  destination: 'public/images',
  /** Dinamizo el nombre del archivo al cargarse. */
  filename: function (req, file, cb) {
    const splitted = file.mimetype.split('/')
    const format = splitted[splitted.length - 1]
    cb(null, `${file.fieldname}_${(new Date()).getTime()}.${format}`)
  }
})

/** Inicializo multer. */
const _upload = multer({
  storage: _storage,
  /**
   * Aca podes agregar validaciones para los archivos
   * que se van a cargar.
   */
  limits: {
    fieldSize: 1024 * 1024 * 5 // Ejemplo size maximo por archivo
  },
  /**
   * Funcion encargada de filtrar y validar el archivo. Se
   * ejecutara por cada archivo que se cargue.
   */
  fileFilter: (req, file, cb) => {
    /**
     * Delimito que el archivo cargado sea una imagen. Caso
     * contrario, retorno un error.
     */
    if (!['image/png', 'image/jpg', 'image/jpeg'].includes(file.mimetype)) {
      return cb(new Error('Invalid mime type'))
    }
    cb(null, true)
  }
})

/**
 * Creo el middleware que ofrece multer, pasando como
 * parametro el nombre del campo donde voy a recibir
 * la UNICA imagen.
 */
const singleUploadMidd = _upload.single('image')
/**
 * Creo el middleware que ofrece multer, pasando como
 * parametro el nombre del campo donde voy a recibir
 * EL CONJUNTO de imagenes.
 */
const arrayUploadMidd = _upload.array('images')

/**
 * Exporto las funciones para reutilizar en el
 * server.
 */
module.exports = {
  singleUploadMidd,
  arrayUploadMidd
}
