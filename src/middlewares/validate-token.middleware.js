/**
 * Ejemplo (muy vago) de como validar un token de login.
 * Los token se usan para validar que el usuario utilizado
 * se encuentra activo.
 *
 * MAS INFO: https://www.npmjs.com/package/jsonwebtoken
 *
 * Cosas que se deberian tener en cuenta:
 *  - Desencriptar el token (Que se encripta cuando te logueas)
 *  - Que el token exista.
 *  - Que el token sea valido.
 *  - Que el usuario no este bloqueado.
 *  - (Opcional) Que el usuario tenga el rol correcto para acceder a estas funciones.
 *  - (Opcional) Que la sesion no este vencida.
 *
 * PD: Siempre se devuelve "token invalido", no hay que agregar info
 * adicional (es la misma logica que el login, siempre se devuelve
 * "usuario/contraseña invalido" sin importar que campo este mal).
 *
 */
const validateTokenMidd = (req, res, next) => {
  /**
   * TODO: Aca deberian estar todas las demas validaciones, en este ejemplo
   * solo se valida que existe el token (flojera).
   */
  if (!req.headers.token) {
    res.status(403).send({
      message: 'Acceso denegado',
      error: 'Token invalido'
    })
  }
  /**
   * Una vez que esta todo ok, podria utilizar el "usuario destokenizado"
   * para pasarlo como informacion adicional en el req, para que los handlers
   * que se ejecuten despues puedan obtener esa data.
   */
  // const detokenized = detokenize(req.headers.token);
  res.locals.userMetadata = { user: 'myUser' } // = detokenized;
  /** Si todo va bien, se pasa al siguiente handler con la funcion "next" de express. */
  next()
}

module.exports = validateTokenMidd
