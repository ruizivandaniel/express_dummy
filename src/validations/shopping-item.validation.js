const Joi = require('joi')

/**
 * Ejemplo de schema de validacion de la
 * libreria "Joi". Esta nos permite realizar
 * la validar facilmente un objeto y en caso
 * de que alguno de los campos este invalido
 * retornará un error con el mensaje
 * correspondiente.
 */
const shoppingItemSchema = Joi.object({
  name: Joi.string().min(10).required(),
  fileName: Joi.string().required(),
  price: Joi.number().min(1).required(),
  stock: Joi.number().min(1).required()
})

module.exports = shoppingItemSchema
